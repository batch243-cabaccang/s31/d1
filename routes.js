const http = require("http");

const server = http.createServer((req, res) => {
  if (req.url == "/greeting") {
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("Ano man sayo, huh?");
    return;
  }
  if (req.url == "/homepage") {
    res.writeHead(200, { "Content-Type": "text/plain" });
    res.end("Home Page");
    return;
  }
  res.writeHead(404, { "Content-Type": "text/plain" });
  res.end("Looking for Something?");
});
server.listen(4000);

console.log("Port 3000");
